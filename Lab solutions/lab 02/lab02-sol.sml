(* summorial : int -> int *)
(* REQUIRES n >= 0 *)
(* ENSURES summorial n returns the sum of the integers 0 through n. *)
fun summorial (n : int) : int =
    if n=0 then 0 else n + summorial(n-1)

(* Tests *)
val 0 = summorial(0)
val 10 = summorial(4)
val 15 = summorial(5)


(* log : int -> int *)
(* REQUIRES n > 0  *)
(* ENSURES log n returns the integer logarithm in base 2 of n. *)
fun log (n : int) : int =
    if n=1 then 0 else 1 + log(n div 2)

(* Tests *)
val 0 = log(1)
val 3 = log(10)
val 4 = log(16)


(* GCD: int * int -> int *)
(* REQUIRES m, n >= 0    *)
(* ENSURES GCD (m, n) returns the g.c.d. of m and n *)
fun GCD (m: int, 0): int = m
  | GCD (0, n: int): int = n
  | GCD (m: int, n: int): int =
        if m > n then GCD(m mod n, n) else GCD(m, n mod m)

(* Tests *)
val 0 = GCD(0, 0)
val 23 = GCD(0, 23)
val 7 = GCD(14, 21)

(* stein : int * int -> int *)
(* REQUIRES m, n > 0        *)
(* ENSURES stein(m,n) returns the g.c.d. of m and n. *)
fun stein(m,n) =
    if m=n then m else
       if m mod 2 = 0
       then
           if n mod 2 = 0
           then 2 * stein(m div 2, n div 2)
           else stein(m div 2, n)
       else if n mod 2 = 0
            then stein(m, n div 2)
            else if n>m
                 then stein(m, (n-m) div 2)
                 else stein((m-n) div 2, n)

(* Tests *)
val 7 = stein(14, 21)
val 9 = stein(36,45)
val 1 = stein(7,9)

(* stein' : int * int -> int *)
(* REQUIRES m, n > 0        *)
(* ENSURES stein'(m,n) returns the g.c.d. of m and n. *)
fun stein'(m,n) =
    if m=n then m
    else case(m mod 2, n mod 2) of
              (0, 0) => 2 * stein(m div 2, n div 2)
            | (0, 1) => stein(m div 2, n)
            | (1, 0) => stein(m, n div 2)
            | (1, 1) => if n>m then stein(m, (n-m) div 2)
                        else stein((m-n) div 2, n)

(* Tests *)
val 7 = stein'(14, 21)
val 9 = stein'(36,45)
val 1 = stein'(7,9)
