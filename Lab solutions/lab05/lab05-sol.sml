exception Unimplemented

(* -------------------- Task 2 -------------------- *)

(* Task 2.1 : DOCUMENT, IMPLEMENT, and TEST this function:
 * evens : int -> bool
 * REQUIRES:
 * ENSURES: *)
val even : int -> bool = fn n => n mod 2 = 0

(* Tests *)
val true = even 292
val false = even 23
val true = even 0

(* Task 2.2 : DOCUMENT, IMPLEMENT, and TEST this function:
 * abs : int -> int
 * REQUIRES:
 * ENSURES: *)
val abs : int -> int = fn n => if n >= 0 then n else ~n

(* Tests *)
val 1 = abs ~1
val 10 = abs 10
val 0 = abs 0

(* -------------------- Task 3 -------------------- *)

(* Task 3.1 : DOCUMENT, IMPLEMENT, and TEST this function :
 * findeven : int list -> int option
 * REQUIRES:
 * ENSURES: *)
fun findeven (L : int list) : int option =
    case L of
      [] => NONE
    | x::xs => (if even x then SOME x else findeven xs)

(* Tests *)
val SOME 2 = findeven [1,2,3]
val NONE = findeven [1,13,5]
val NONE = findeven []
val SOME 2 = findeven [2,4,8]

(* -------------------- Task 4 -------------------- *)

(* foldr : ('a * 'b -> 'b) -> 'b -> 'a list -> 'b
 * ENSURES: Given a function f, an identity e, and a list L = [a_0 ... a_n],
 * foldr f e L evaluates to f(a_0, f(a_1, f(a_2, ... f(a_n, e) ... ))), or e
 * if the list is empty. foldr stands for "fold right" *)
fun foldr (f : 'a * 'b -> 'b) (e : 'b) (L : 'a list) : 'b =
    case L of
      []    => e
    | x::xs => f (x, foldr f e xs)

(* sum : int list -> int
 * ENSURES: sum L evaluates to the sum of the elements in L *)
fun sum (L : int list) : int =
    case L of
      []    => 0
    | x::xs => x + (sum xs)

(* sum' : int list => int
 * ENSURES: sum' L evaluates to the sum of the elements in L *)
fun sum' (L : int list) : int = foldr (fn (x,y) => x + y) 0 L

(* Task 4.1 : DOCUMENT, IMPLEMENT, and TEST this function: *)
(* exists : ('a -> bool) -> 'a list -> bool
 * REQUIRES:
 * ENSURES: *)
fun exists (p : 'a -> bool) (L : 'a list) : bool =
    foldr (fn (a,b) => p a orelse b) false L

(* Tests *)
val true = exists even [1,2,3]
val false = exists even []
val false = exists even [1,1,1]

(* Task 4.2 : DOCUMENT, IMPLEMENT, and TEST this function: *)
(* forall : ('a -> bool) -> 'a list -> bool
 * REQUIRES:
 * ENSURES: *)
fun forall (p : 'a -> bool) (L : 'a list) : bool =
    foldr (fn (a,b) => p a andalso b) true L

(* Tests *)
val true = forall even [2,2,2]
val true = forall even []
val false = forall even [2,4,8,11]

(* -------------------- Task 5 -------------------- *)

(* Polymorphic tree datatype representing a tree with data ONLY at leaves *)
datatype 'a tree = Emp
                 | Leaf of 'a
                 | Branch of 'a tree * 'a tree

(* Task 5.2 : DOCUMENT, IMPLEMENT, and TEST this function:
 * full : ('a * int) -> int tree
 * REQUIRES:
 * ENSURES: *)
fun full (a : 'a, n : int) : 'a tree =
    case n of
      0 => Leaf a
    | _ => Branch(full (a,n-1), full (a,n-1))

(* Tests *)
val (Leaf "hi") = full ("hi",0)
val (Branch (Branch (Leaf 4,Leaf 4),Branch (Leaf 4,Leaf 4))) = full (4,2)
val (Branch (Leaf ":)", Leaf ":)")) = full (":)",1)

(* Task 5.3 DOCUMENT, IMPLEMENT, and TEST this function:
 * treemap : ('a -> 'b) -> ('a tree -> 'b tree)
 * REQUIRES:
 * ENSURES: *)
fun treemap (f : 'a -> 'b) (t : 'a tree) : 'b tree =
    case t of
      Emp          => Emp
    | Leaf a       => Leaf (f a)
    | Branch (l,r) => Branch (treemap f l, treemap f r)

(* Tests *)
val Branch (Branch (Leaf 1,Leaf 1),Branch (Leaf 0,Leaf 0)) =
    treemap (fn x => x div 3) (Branch(Branch(Leaf 4, Leaf 5),
                                      Branch (Leaf 2, Leaf 0)))
val Emp = treemap map Emp
val Leaf 3 = treemap (fn x => x + 3) (Leaf 0)
val (Branch (Leaf 1, Leaf 1)) = treemap (fn x => 1)
                                        (Branch (Leaf ["hi", "hi"],
                                                 Leaf ["","a","b"]))

(* Task 5.5 : DOCUMENT, IMPLEMENT, and TEST this function:
 * treefold : ('a * 'a -> 'a) -> 'a -> 'a tree -> 'a
 * REQUIRES:
 * ENSURES: *)
fun treefold (f : 'a * 'a -> 'a) (e : 'a) (t : 'a tree) : 'a =
    case t of
      Emp          => e
    | Leaf a       => a
    | Branch (l,r) => f (treefold f e l, treefold f e r)

(* Tests *)
val 10 = treefold (fn(a,b) => a+b) 0 (Branch (Leaf 4, Branch (Leaf 3, Leaf 3)))
val "Hi there!" = treefold (fn(s,t) => s^t) "" (Branch (Leaf "Hi ",
                                                        Leaf "there!"))
val 0 = treefold (fn (a,b) => 0) 0 (Branch (Leaf 4, Leaf 3))
val 0 = treefold (fn (a,b) => a div b) 0 Emp
val 4 = treefold (fn (a,b) => a * b) 1 (Leaf 4)

(* Task 5.7 : DOCUMENT, IMPLEMENT, and TEST this function:
 * addtoleaves : int -> int tree -> int tree
 * REQUIRES:
 * ENSURES: *)
fun addtoleaves (n :int) (t : int tree) : int tree =
    treemap (fn x => x + n) t

(* Tests *)
val (Branch (Leaf 7, Leaf 2)) = addtoleaves 4 (Branch (Leaf 3, Leaf ~2))
val Emp = addtoleaves 3 Emp
val (Branch(Emp, Leaf 19)) = addtoleaves 9 (Branch(Emp, Leaf 10))

(* Task 5.8 : DOCUMENT, IMPLEMENT, and TEST this function:
 * sumleaves : int tree -> int
 * ENSURES: given an int tree t, sumleaves t evaluates to the sum of the
 * values at the leaves of t *)
fun sumleaves (t : int tree) : int =
    treefold (fn (x,y) => x + y) 0 t

(* Tests *)
val 10 = sumleaves (Branch (Emp, Branch(Branch(Leaf 4, Emp), Leaf 6)))
val 0 = sumleaves Emp
val 1 = sumleaves (Leaf 1)
val 10 = sumleaves (Branch (Emp, Branch(Leaf 10, Emp)))
