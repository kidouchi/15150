use "lib.sml";

val compare = Int.compare

(* Task 2.3 *)
(* cmpmod : int * int -> order *)
(* REQUIRES : a,b non-negative and type int *)
(* ENSURES : comparison of least significant digit between a & b *)
fun cmpmod (a: int, b: int) : order = 
   compare(a mod 10,b mod 10)

val GREATER = cmpmod(2,1)
val GREATER = cmpmod(23,12)
val GREATER = cmpmod(223,12)
val EQUAL = cmpmod(1,1)
val EQUAL = cmpmod(21,31)
val EQUAL = cmpmod(421,31)
val LESS = cmpmod(1,2)
val LESS = cmpmod(11,22)
val LESS = cmpmod(121,23)

(* cmpdiv : int * int -> order *)
(* REQUIRES : a,b non-negative and type int *)
(* ENSURES : comparison of most significant digit between a & b *)
fun cmpdiv (a: int, b: int) : order =
   compare(a div 10, b div 10)

val GREATER = cmpdiv(31,21)
val GREATER = cmpdiv(231,221)
val GREATER = cmpdiv(4526,236)
val EQUAL = cmpdiv(22,23)
val EQUAL = cmpdiv(2,2)
val EQUAL = cmpdiv(3,2)
val EQUAL = cmpdiv(231,234)
val LESS = cmpdiv(22,32)
val LESS = cmpdiv(45,231)
val LESS = cmpdiv(231,241)

(* Task 4.1 *)
(* bst : ('a * 'a -> order) -> 'a tree -> bool *)
(* REQUIRES : cmp compares two values of type 'a in t of type 'a tree *) 
(* ENSURES : will check that t is cmp-binary-search tree or not *)
fun bst (cmp: 'a * 'a -> order) (t: 'a tree) : bool =
   case t of
      Empty => true
   |  Node(Empty,x,Empty) => true
   |  Node(Empty,x,Node(t1,y,t2)) =>
         cmp(x,y) = LESS andalso bst cmp (Node(t1,y,t2) )
   |  Node(Node(t1,y,t2),x,Empty) =>
         cmp(x,y) = GREATER andalso bst cmp (Node(t1,y,t2))
   |  Node(Node(t1,y,t2),x,Node(t3,z,t4)) =>
         cmp(x,y) = GREATER andalso bst cmp (Node(t1,y,t2))
         andalso cmp(x,z) = LESS andalso bst cmp (Node(t3,z,t4))

val true = bst compare (Empty)
(*         6
         /   \
        4     8  *)
val t1 = Node(Empty,4,Empty)
val t2 = Node(Empty,8,Empty)
val t3 = Node(t1,6,t2)
val true = bst compare (t3)
(*      6
      /   \
     8     4  *)
val ta = Node(t2,6,t1)     
val false = bst compare (ta)
(*     6
     /   \
    4     8
   /
  3            *)
val t4 = Node(Empty,3,Empty)
val t5 = Node(t4,4,Empty)
val t6 = Node(t5,6,t2)
val true = bst compare (t6)
(*      6
      /   \
     4     8
      \
       3          *)
val t7 = Node(Empty,4,t4)
val t8 = Node(t7,6,t2)
val false = bst compare (t8)
(*       6
       /   \
      4     8
    /   \
   3     5       *)
val t9 = Node(Empty,5,Empty)
val t10 = Node(t4,4,t9)
val t11 = Node(t10,6,t2)
val true = bst compare(t11)
(*       6
       /   \
      4     8
    /   \ 
   5     3        *)
val tf = Node(t9,4,t4) 
val tg = Node(tf,6,t2)
val false = bst compare (tg)
(*       6
       /   \
      4     8
             \ 
              9    *)
val tx = Node(Empty,9,Empty)
val tz = Node(Empty,8,tx)
val tw = Node(t1,6,tz)
val true = bst compare(tw)
(*    6
    /   \
   4     8
        /
       9   *)
val t12 = Node(tx,8,Empty)
val t13 = Node(t1,6,t12)
val false = bst compare (t13)
(*     6
     /   \
    4     8
         / \
        7   9     *) 
val t14 = Node(Empty,7,Empty)
val t15 = Node(t14,8,tx)
val t16 = Node(t1,6,t15)
val true = bst compare (t16)
(*       6
       /   \
      4     8
          /   \
         9     7   *)
val t17 = Node(tx,8,t14)
val t18 = Node(t1,6,t17)
val false = bst compare (t18)
(*          6
          /   \
         4     8
       /  \   /  \
      3    5 7    9  *)
val t19 = Node(t10,6,t15)
val true = bst compare (t19)

(* Task 4.2 *)
(* lookup : ('a * 'a -> order) -> 'a * 'a tree -> 'a option *)
(* REQUIRES : for all types t, comparisons cmp, and x type t, and cmp-binary search tree t type t tree *)
(* ENSURES : Finds whether x is in t *)
fun lookup (cmp: 'a * 'a -> order) (x: 'a, t: 'a tree) : 'a option =
   case (x,t) of
      (x,Empty) => NONE
   |  (x,Node(t1,y,t2)) =>
         if cmp(x,y) = EQUAL then SOME(y)
         else if cmp(x,y) = GREATER then
            case lookup cmp (x,t2) of 
               NONE => NONE
            |  SOME(z) => SOME(z)
         else
            case lookup cmp (x,t1) of
               NONE => NONE
            |  SOME(z) => SOME(z)

val SOME(6) = lookup compare (6,t3)
val NONE = lookup compare (5,t3)
val NONE = lookup compare (6,Empty)
val SOME(3) = lookup compare (3,t19)
val NONE = lookup compare (1,t19)

(* Task 4.3 *)
(* insert : ('a * 'a -> order) -> 'a * 'a tree -> 'a tree *)
(* REQUIRES : for all types t, all comparisons cmp, and x type t and cmp-bstt of type t tree *)
(* ENSURES : will insert x in t and result will still be cmp-bst *)
fun insert (cmp: 'a * 'a -> order) (x: 'a, t: 'a tree) : 'a tree =
   case (x,t) of
      (x,Empty) => Node(Empty,x,Empty)
   |  (x,Node(t1,y,t2)) =>
         if cmp(x,y) = GREATER then
            Node(t1,y,insert cmp (x,t2))       
         else if cmp(x,y) = LESS then
            Node(insert cmp (x,t1),y,t2)
         else 
            Node(t1,x,t2)

(* All previous and resulting trees have been proven to be cmp-binary search trees in the bst tests *)
val Node(Empty,5,Empty) = insert compare (5,Empty)
val t6 = insert compare (3,t3) 
val tw = insert compare (9,t3)
val t11 = insert compare (5,t6)
val t16 = insert compare (7,tw)

(* Task 5.1 *)
(* dict_cmp : entry * entry -> order *)
(* REQUIRES : a,b of type entry *)
(* ENSURES : Compares words, not definitions, in a,b *)
fun dict_cmp (a: entry, b: entry) : order = 
   let val (x,y) = a 
       val (v,w) = b
   in String.compare(x,v)
   end
   
val GREATER = dict_cmp(("hello","happy greeting"),("bye","sad greeting"))
val LESS = dict_cmp(("bye","sad greeting"),("hello","happy greeting"))
val EQUAL = dict_cmp(("bye","sad greeting"),("bye","farewell"))

(* Task 5.2 *)
(* build_dictionary : entry list -> dict *)
(* REQUIRES : *)
(* ENSURES : *)
fun build_dictionary (entries : entry list) : dict = 
   foldr (insert dict_cmp) Empty entries

val buildtree = (fn x => Node(Empty,x,Empty))
val s1 = buildtree(("hello","greeting"))
val s2 = buildtree(("bye","goodbye"))
val s3 = buildtree(("cat","pet"))
val l1 = [("hello","greeting"),("bye","goodbye")]
val l2 = rev(l1)
val l3 = l1 @ [("cat","pet")]

val Node(Empty,("bad","not good"),Empty) = 
   build_dictionary([("bad","not good")])
val Node(Empty,s2,s1) = build_dictionary(l1)
val Empty = build_dictionary([ ])
val Node(s2,s1,Empty) = build_dictionary(l2)
val Node(s2,s3,s1) = build_dictionary(l3)

(* Task 5.3 *)
(* find_meaning : string * dict -> string option *)
(* REQUIRES : for all "words" s type string, and d type dict *) 
(* ENSURES : will give definition of s *)
fun find_meaning (" ": string, d: dict) : string option = NONE 
   |  find_meaning(e,Empty) = NONE
   |  find_meaning(e,d) =  
         case lookup dict_cmp ((e," "),d) of
            NONE => NONE
         |  SOME(x,y) => SOME(y)

val SOME("pet") = find_meaning("cat",build_dictionary(l3))
val NONE = find_meaning(" ",build_dictionary(l3))
val NONE = find_meaning("cat", Empty)
val SOME("greeting") = find_meaning("hello",build_dictionary(l3))





